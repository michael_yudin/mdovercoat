Pod::Spec.new do |s|
  s.name     = 'MDOvercoat'
  s.version  = '2.1.2'
  s.license  = 'MIT'
  s.summary  = 'Overcoat is a small but powerful library that makes creating REST clients simple and fun.'
  s.homepage = 'https://github.com/gonzalezreal/Overcoat'
  s.authors  = { 'Guillermo Gonzalez' => 'gonzalezreal@icloud.com' }
  s.social_media_url = 'https://twitter.com/gonzalezreal'
  s.source   = { :git => 'https://michael_yudin@bitbucket.org/michael_yudin/mdovercoat.git', :tag => '2.1.2' }

  s.requires_arc = true
  
  s.ios.deployment_target = '6.0'
  s.osx.deployment_target = '10.8'
  
  s.default_subspec = 'NSURLSession'
  
  s.subspec 'Core' do |ss|
    ss.dependency 'AFNetworking', '~> 2.5'
    ss.dependency 'Mantle', '~> 1.5'
    
    ss.public_header_files = 'Overcoat/*.h'
    ss.source_files = 'Overcoat/Overcoat.h', 'Overcoat/OVCResponse.{h,m}', 'Overcoat/NSError+OVCResponse.{h,m}', 'Overcoat/OVCURLMatcher.{h,m}',
                      'Overcoat/OVC{ModelResponse,SocialRequest}Serializer.{h,m}', 'Overcoat/OVCManagedStore.{h,m}', 'Overcoat/OVCHTTPRequestOperationManager.{h,m}',
                      'Overcoat/OVCManagedObjectSerializingContainer.h', 'Overcoat/NSDictionary+Overcoat.{h,m}'
    ss.frameworks = 'Foundation', 'Accounts', 'Social', 'CoreData'
  end
    
  s.subspec 'NSURLSession' do |ss|
    ss.dependency 'MDOvercoat/Core'
    ss.source_files = 'Overcoat/OVCHTTPSessionManager.{h,m}'
  end
  
  s.subspec 'PromiseKit' do |ss|
    ss.dependency 'MDOvercoat/Core'
    ss.dependency 'MDOvercoat/NSURLSession'
    ss.dependency 'PromiseKit', '~>1.2'
    
    ss.public_header_files = 'PromiseKit+Overcoat/*.h'
    ss.source_files = 'PromiseKit+Overcoat'
  end

  s.subspec 'ReactiveCocoa' do |ss|
    ss.dependency 'MDOvercoat/Core'
    ss.dependency 'MDOvercoat/NSURLSession'
    ss.dependency 'ReactiveCocoa', '~>2.4'
    
    ss.public_header_files = 'ReactiveCocoa+Overcoat/*.h'
    ss.source_files = 'ReactiveCocoa+Overcoat'
  end

end
